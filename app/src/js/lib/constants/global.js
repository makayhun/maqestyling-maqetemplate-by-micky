const API_PROD_URL = 'http://maqe.github.io/json/'
const API_DEV_URL = 'http://maqe.github.io/json/'

let currentUrl = API_DEV_URL
let currentEnv = 'development'

if (process.env.NODE_ENV === 'production') {
  currentUrl = API_PROD_URL
  currentEnv = 'production'
}

export const API_ROOT = currentUrl;
export const ENVIRONMENT = currentEnv;
