import React from 'react'
import { browserHistory } from 'react-router'
import styles from './styling.scss'
export default class MaqeStylingContainer extends React.Component {
  render() {
    return (

      <div id="MaqeStylingContainer" >
        <button onClick={() => browserHistory.push('/maqeforum')} className='goto' > go to MaqeForum</button>
        <div className="wrapBlock" >
          <div className="blockContainer" >
            <div className="wrapCenterBlock" >
              <div className="centerBlock" ></div>
            </div>
            <div style={{ display: 'flex' }}>
              <div className="characterContianer " >M</div>
              <div className="characterContianer " >A</div>
            </div>
            <div style={{ display: 'flex' }}>
              <div className="characterContianer " >Q</div>
              <div className="characterContianer " >E</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
