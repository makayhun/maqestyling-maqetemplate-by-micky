import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ReactPaginate from 'react-paginate';
import moment from 'moment'
import { browserHistory } from 'react-router'
import styles from './forums.scss'
import { getPosts, getAuthors } from '../../actions/Forum'

export class PostList extends React.Component {

  timeDifference(elapsed) {
    const msPerMinute = 60 * 1000;
    const msPerHour = msPerMinute * 60;
    const msPerDay = msPerHour * 24;
    const msPerMonth = msPerDay * 30;
    const msPerYear = msPerDay * 365;

  //  const elapsed = current - previous;

    if (elapsed < msPerMinute) {
      return `${Math.round(elapsed / 1000)} seconds ago`;
    } else if (elapsed < msPerHour) {
      return `${Math.round(elapsed / msPerMinute)} minutes ago`;
    } else if (elapsed < msPerDay) {
      return `${Math.round(elapsed / msPerHour)} hours ago`;
    } else if (elapsed < msPerMonth) {
      return `${Math.round(elapsed / msPerDay)} days ago`;
    } else if (elapsed < msPerYear) {
      return `${Math.round(elapsed / msPerMonth)} months ago`;
    } else {
      return `${Math.round(elapsed / msPerYear)} years ago`;
    }
  }
  render() {
    const Authors = this.props.authors
    let evenBackground

    const Posts = this.props.posts.map((post, index) => {
      if ((index + 1) % 2 == 0) {
        evenBackground = 'evenBackground'
      } else {
        evenBackground = 'oddBackground'
      }
      return (
        <div key={index} className={evenBackground}>
          <div className='postContainer' >
            <img className='postImg' src={post.image_url}></img>
            <div className='postDetail' >
              <div className='title' >{post.title}</div>
              <div className='detail' >{post.body}</div>
              <div className='create' >{this.timeDifference((moment().unix() - moment(post.created_at).unix()) * 1000)}</div>
            </div>
          </div>

          <div className='authorContainer' >
            <div>
              <img className='authorImg' src={Authors[post.author_id - 1].avatar_url}></img>
              <div className='authorName' >{Authors[post.author_id - 1].name}</div>
              <div className='authorRole' >{Authors[post.author_id - 1].role}</div>
              <div className='authorPlace' >{Authors[post.author_id - 1].place}</div>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className='postlist' >
        {Posts}
      </div>
    );
  }
}


export class ForumsContainer extends React.Component {
  componentWillMount() {
    this.props.getPosts()
    this.props.getAuthors()
  }
  render() {
    return (
      <div id="ForumsContainer" >
        <button onClick={() => browserHistory.push('/MaqeStyling')} className='goto' > go to MaqeStyling</button>
        <div className="title" >
          MAQE Forums
        </div>
        <div className="subtitle" >
          Subtitle
        </div>
        <div className="posts" >
          Posts
        </div>
        <div style={{ width: 1225 }}>
          {this.props.authors != '' ? <PostList posts={this.props.posts} authors={this.props.authors} /> : null}
          <ReactPaginate previousLabel={'Previous'}
            nextLabel={'Next'}
            breakLabel={<a href="">...</a>}
            breakClassName={'break-me'}
            pageCount={8}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
                    //   onPageChange={this.handlePageClick}
            containerClassName={'pagination-forums'}
            subContainerClassName={'pages pagination'}
            activeClassName={'active'} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  authors: state.forums.authors,
  posts: state.forums.posts
})
const mapDisPatchToProps = dispatch => ({
  getPosts: bindActionCreators(getPosts, dispatch),
  getAuthors: bindActionCreators(getAuthors, dispatch)
})

export default connect(
  mapStateToProps,
  mapDisPatchToProps
)(ForumsContainer)
