import {  LOAD_POST_REQUEST, LOAD_POST_SUCCESS, LOAD_POST_FAILURE,
          LOAD_AUTHOR_REQUEST, LOAD_AUTHOR_SUCCESS, LOAD_AUTHOR_FAILURE }
from './actionTypes'
import { API_ROOT } from '../lib/constants/global'
import { CALL_API } from 'redux-api-middleware';

export function getPosts() {
  return {[CALL_API]:  {
    types: [LOAD_POST_REQUEST, LOAD_POST_SUCCESS, LOAD_POST_FAILURE],
    endpoint: `${API_ROOT}/posts.json`,
    method: 'GET'
  }
}
}

export function getAuthors() {
  return {[CALL_API]:  {
    types: [LOAD_AUTHOR_REQUEST, LOAD_AUTHOR_SUCCESS, LOAD_AUTHOR_FAILURE],
    endpoint: `${API_ROOT}/authors.json`,
    method: 'GET'
  }
}
}
