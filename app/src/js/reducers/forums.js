// import assign from 'lodash/assign'
import { LOAD_POST_REQUEST, LOAD_POST_SUCCESS, LOAD_POST_FAILURE,
          LOAD_AUTHOR_REQUEST, LOAD_AUTHOR_SUCCESS, LOAD_AUTHOR_FAILURE }
from '../actions/actionTypes'

export const initialState = {
posts:[],
authors:[]
}

function forums(state = initialState, action) {
    //console.log(2222,action.payload);
  switch(action.type) {

    case LOAD_POST_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      })

    case LOAD_POST_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        posts: action.payload
      })

    case LOAD_POST_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      })

    case LOAD_AUTHOR_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      })

    case LOAD_AUTHOR_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        authors: action.payload
      })

    case LOAD_AUTHOR_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      })
    default:
      return state
  }
}
export default forums;
