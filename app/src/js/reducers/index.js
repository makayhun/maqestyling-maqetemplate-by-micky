import { combineReducers } from 'redux'
import entities from './entities'
import error from './error'
import loading from './loading'
import forums from './forums'

const rootReducer = combineReducers({
	entities,
	error,
	loading,
	forums
});

export default rootReducer
