import React from 'react'
import { Router, Route, IndexRoute, IndexRedirect, hashHistory, browserHistory } from 'react-router'
import App from '../containers/App'
import MaqeForum from '../containers/MaqeForum'
import MaqeStyling from '../containers/MaqeStyling'
import ErrorPage from '../components/ErrorPage'


const routes = (
  <Router history={browserHistory} >
    <Route path="/" component={App}>
      <IndexRoute path="/maqeforum" component={MaqeForum} />
      <Route path="/maqeforum" component={MaqeForum} />
      <Route path="/maqestyling" component={MaqeStyling} />
      <Route path="*" component={ErrorPage} />
    </Route>
  </Router>
)

export default routes
